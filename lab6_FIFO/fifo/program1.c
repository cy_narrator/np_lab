#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#define buffer_size 80

void error(char* message)
{
    fprintf(stderr, "\n%s\n", message);
    exit(-1);
}

void main()
{
    char write_buffer[buffer_size], read_buffer[buffer_size];
    char* myfile;
   // FILE* fd;
   int fd;
    mkfifo("myfile", 0777);
    fd = open("myfile", O_WRONLY);
    fgets(write_buffer, buffer_size, stdin);
    write(fd, write_buffer,buffer_size);


    fd = open("myfile", O_RDONLY);
    read(fd, read_buffer, buffer_size);
    printf("\nText from program 2 is %s", read_buffer);

}