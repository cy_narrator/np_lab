#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

void error(char* message)
{
    fprintf(stderr, "\n%s\n", message);
    exit(-1);
}

void main()
{
    int my_pipe[2];
    pid_t pid; // PID has the datatype of pid_t defined in <sys/types.h> but printed as %d(integer) by printf. But it can also be declared with int datatype
    char buffer[80];
    if(pipe(my_pipe) == -1)
    {
        error("Pipe could not be created"); // Above written function error()
    }

    pid = fork();
    if(pid < 0)
    {
        error("Child process could not be created");
    }
    else if(pid == 0)
    {
        printf("\nPlease enter the text: ");
        fgets(buffer, 80, stdin);
        close(my_pipe[0]);
        write(my_pipe[1], buffer, 80);
    }
    else
    {
        close(my_pipe[1]);
        read(my_pipe[0], buffer, 80);
        printf("The child written in pipe is: %s", buffer);
    }
}