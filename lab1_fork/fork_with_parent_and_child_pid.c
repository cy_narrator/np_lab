#include<stdio.h>
#include<unistd.h>
void main()
{
	pid_t process_id;
	process_id = fork();
	if(process_id < 0)
	{
		printf("New process cannot be created");
	}

	else if(process_id > 0)
	{
		printf("I am in parent and parent pid is %d", getpid());
		printf("Child process id is %d", process_id);
	}

	else
	{
		printf("I am child and pid is %d", getppid());
	}
}
