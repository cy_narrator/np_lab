#include<stdio.h>
#include<sys/socket.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<netinet/in.h>
#include<netdb.h>

int main(int argc, char *argv[])
{
	int sockfd, portno, conn;
	char buffer[256];
	int buf;	
	struct sockaddr_in server;
	struct hostent *server1;

	if(argc<3)
	{
		perror("Not provided all the information.");
		exit(1);
	}

	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)
	{
		perror("could not create socket.");
		exit(1);
	
	}

	server1=gethostbyname(argv[1]);
	if(server1==NULL)
	{
		perror("No such host is available");
		exit(1);
	}
	
	
	memset((char *)&server, 0, sizeof(server));
	server.sin_family=AF_INET;
	memcpy((char *)server1->h_addr,(char *)&server.sin_addr.s_addr,server1->h_length);

	
	portno=atoi(argv[2]);
	server.sin_port=htons(portno);
	

	conn=connect(sockfd,(struct sockaddr *)&server,sizeof(server));
	if(conn<0)
	{
		perror("could not connect server.");
		exit(1);
	}
	printf("Please enter the message: ");

	memset(buffer,0,sizeof(buffer));
	fgets(buffer,256,stdin);

	buf=write(sockfd,buffer,strlen(buffer));
	if(buf<0)
	{
		perror("error in writing.");
		exit(1);
	}
	
	memset(buffer,0,sizeof(buffer));
	buf=read(sockfd,buffer,256);
	if(buf<0)
	{
		perror("error in writing.");
		exit(1);
	}
	printf("%s\n",buffer);	
	return 0;
}

