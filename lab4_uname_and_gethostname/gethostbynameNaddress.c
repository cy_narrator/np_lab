#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h> //for inet_aton() function
#include <stdlib.h> //for exit() function
int main()
{
	int i;
	struct hostent *he;
	struct in_addr addr;

	// get the address of www.cit.edu.np

	he = gethostbyname("www.cit.edu.np");
	if (he == NULL) { // do some error checking
	    herror("error in gethostbyname"); // herror(), NOT perror()
	    exit(1);
	}

	// print information about this host
	printf("Official name is: %s\n", he->h_name);
	printf("IP address: %s\n", inet_ntoa(*(struct in_addr*)he->h_addr));
	

	// get the host name of 66.94.230.32

	inet_aton("192.81.170.7", &addr);
	he = gethostbyaddr(&addr, sizeof(addr), AF_INET);

	printf("Host name: %s\n", he->h_name);
	return 0;
}
