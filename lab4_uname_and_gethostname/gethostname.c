#include <stdio.h> //for printf() function
#include <sys/unistd.h> //for gethostname() function 

int main()
{
	char hostname[128];	
	gethostname(hostname, sizeof(hostname));
	printf("My hostname: %s\n", hostname);
	return 0;
}