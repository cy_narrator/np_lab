# Must use UNIX or GNU/Linux and not Windows, may work in Mac too

First compile the program then run it with ./<program_name>

to compile,
    gcc <source.c> -o <executable_file_name>

to run,
    ./<executable_file_name>

If you do not have gcc or have problems compiling, try installing build-essential in Debian/Ubuntu systems and base-devel in Arch based systems. Otherwise it should work.

You may also use clang in place of gcc if thats your thing
