#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

int main()
{
	int qid,len;
	char str[30];
	struct
	{
		long mtype;
		char mtext[40];
	}buff;
	qid=msgget((key_t)100,IPC_CREAT|0666);
	if(qid==-1)
	{
		printf("Message get Failed");
		exit(1);
	}
	if(msgrcv(qid,&buff,30,2,0)<0)
	{
		printf("Message Receive Failed");
	}
	printf("Received Message is %s\n",buff.mtext);
	return 0;
}
