#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
int main()
{
	int qid,len;
	char str[30];
	struct
	{
		long mtype;
		char mtext[30];
	}message;
	qid=msgget((key_t)100,IPC_CREAT|0666);
	if(qid<0)
	{
		printf("\n Message get Failed\n");
		exit(1);
	}
	printf("\n Enter to Message \n\t");
	gets(str);
	len=strlen(str);
	str[len]='\0';
	strcpy(message.mtext,str);
	message.mtype=2;
	if(msgsnd(qid,&message,len+1,0)>0)
	{
		printf("\n Msgsnd Failed");
		exit(1);
	}
	printf("Message %s has been sent",message.mtext);
	return 0;
}
