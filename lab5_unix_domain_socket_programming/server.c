//client server chat application with the help of Unix Domain Stream.

#include<stdio.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/un.h>

int main(int argc, char * argv[])
{
	int sockfd,bindfd,sockmsg,valread;
	struct sockaddr_un server;
	char buffer[256];

	if(argc<2)
	{
		printf("please provide pathname.");
		exit(1);
	}

	sockfd=socket(AF_LOCAL, SOCK_STREAM, 0);
	if(sockfd<0)
	{
		perror("could not open socket.");
		exit(1);
	}

	server.sun_family=AF_LOCAL;
	strcpy(server.sun_path,argv[1]);
	
	bindfd=bind(sockfd, (struct sockaddr *)&server, sizeof(server));
	if(bindfd<0)
	{
		perror("could not bind.");
		exit(1);
	}

	printf("socket has name %s\n",server.sun_path);

	listen(sockfd,5);

	for(;;)
	{
		sockmsg=accept(sockfd,0,0);
		if(sockmsg<0)
		{
			perror("could not accept.");
		}
		else
		{
			do
			{
				memset((char *)buffer,0,sizeof(buffer));
				valread=read(sockmsg,buffer,256);
				if(valread<0)
					perror("no straem to read.");
				else if(valread==0)
					printf("ending connection.\n");
				else
					printf("the message is: %s\n",buffer);				
				
			}
			while(valread>0);
			close(sockmsg);
		}

	}
	close(sockfd);
	unlink("socket");
	return 0;

}
