//client server chat application with the help of Unix Domain Stream.

#include<stdio.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/un.h>

int main(int argc, char *argv[])
{
	int sockfd,connectfd,valwrite;
	struct sockaddr_un client;
	char buffer[256];

	if(argc<2)
	{
		printf("please provide pathname.");
		exit(1);
	}

	sockfd=socket(AF_LOCAL, SOCK_STREAM, 0);
	if(sockfd<0)
	{
		perror("could not open socket.");
		exit(1);
	}

	client.sun_family=AF_LOCAL;
	strcpy(client.sun_path,argv[1]);

	connectfd=connect(sockfd,(struct sockaddr *)&client, sizeof(client));	
	if(connectfd<0)
	{
		close(sockfd);		
		perror("could not connect.");
		exit(1);
	}
		
	printf("Please enter the text to be request to server:");
	fgets(buffer,sizeof(buffer),stdin);
	write(sockfd,buffer,sizeof(buffer));

	close(sockfd);
	return 0;

}
