#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>

int main()
{
	pid_t pid,w;
	int w1,wstatus;
	pid=fork();
	if (pid<0)
	{
		printf("The child could not be created.");
		exit(1);
	}
	if (pid==0)
	{
		//sleep(5);	
		printf("I am child process and process id is: %d",getpid());
		printf("My parent's process id is: %d",getppid());
	}
	else
	{
		printf("I am parent process and process id is: %d",getpid());
		//wait(NULL);
		w=wait(NULL);
		w1=wait(&wstatus);
		printf("status is %d\n",WIFEXITED(wstatus));
		printf("pid is %d\n",w1);
		printf("My child's process id is: %d", pid);
	}
	




return 0;
}
