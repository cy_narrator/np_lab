#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>

int main()
{
	pid_t pid,pid1,pid2,sw;
	int w1,wstatus;
	pid=fork();
	if (pid<0)
	{
		printf("The child could not be created.");
		exit(1);
	}
	if (pid1==0)
	{
		//sleep(5);	
		printf("I am child process and process id is: %d",getpid());
		printf("My parent's process id is: %d",getppid());
	}
	else
	{
		pid2=fork();
		if(pid2==0)
		{
			printf("I am second child process and process id is: %d",getpid());
			printf("Second child parent's process id is: %d",getppid());	
		}
		else
		{
			
			//wait(NULL);
			//w=wait(NULL);
		waitpid(pid1,NULL,0);
		printf("I am parent process and process id is: %d",getpid());
		printf("my first child %d",pid1);
		printf("my second child %d",pid2);
	}
	




return 0;
}
}
